plugins {
    `java-gradle-plugin`
    `kotlin-dsl`
    kotlin("jvm") version "1.6.21"
    kotlin("plugin.serialization") version "1.6.21"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("com.charleskorn.kaml:kaml:0.49.0")
}

gradlePlugin {
    plugins {
        create("armbian") {
            id = "plugin.armbian"
            implementationClass = "plugin.armbian.ArmbianPlugin"
        }
    }
}
