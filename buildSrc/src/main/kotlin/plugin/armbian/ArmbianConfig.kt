package plugin.armbian

import kotlinx.serialization.Serializable

@Serializable
data class ArmbianConfig(
    val board: String,
    val branch: String,
    val release: String,
    val buildMinimal: Boolean = false,
    val buildDesktop: Boolean = false,
    val disableIpv6: Boolean = false,
    val rootFsType: String = "ext4",
    val bootSize: Short = 256,
    val installKernelSource: Boolean = false,
    val includeLegacyWireguard: Boolean = false,
    val additionalPackages: Collection<String> = listOf(),
    val enableOverlays: Collection<String> = listOf(),
    val kernelParameters: Map<String, String> = mapOf(),
)
