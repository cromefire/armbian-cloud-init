package plugin.armbian

import com.charleskorn.kaml.Yaml
import com.charleskorn.kaml.encodeToStream
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Console
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.options.Option
import org.intellij.lang.annotations.Language
import java.io.OutputStream

abstract class InitDeviceTask : DefaultTask() {
    init {
        group = "armbian"
    }

    @set:Option(option = "hostname", description = "The hostname for which to create an empty configuration for")
    @get:Input
    var hostname: String = "";

    @set:Option(option = "board", description = "The name of the board of the device")
    @get:Input
    var board: String = "";

    @TaskAction
    fun initialize() {
        val resources = project.rootProject.file("resources")

        if (hostname.isBlank()) {
            throw IllegalArgumentException("A hostname has to be provided via --hostname")
        }
        if (board.isBlank()) {
            throw IllegalArgumentException("A board name has to be provided via --board")
        }
        if (!hostname.matches(ArmbianConstants.hostnameRegex)) {
            throw IllegalArgumentException("The hostname doesnt satisfy the hostname constraints, please use a valid hostname")
        }

        project.mkdir("devices/$hostname/extra-files")
        project.mkdir("devices/$hostname/init-files")
        project.mkdir("devices/$hostname/overlays")
        project.copy {
            from(
                resources.resolve("customize.sh"),
                resources.resolve("network-config.yaml"),
                resources.resolve("user-data.yaml")
            )
            into("devices/$hostname")
        }
        project.file("devices/$hostname/config.yaml").outputStream().use {
            writeConfig(board, it)
        }

        System.out.println(generateOverview(hostname))
    }

    companion object {
        @Language("yaml")
        fun writeConfig(board: String, stream: OutputStream) {
            val config = ArmbianConfig(board, "current", "jammy")
            Yaml.default.encodeToStream<ArmbianConfig>(config, stream)
        }

        fun generateOverview(hostname: String) = """
            Created the following file for you:
            devices/$hostname/
                extra-files/  # Put any files in here that should be put into the image (e.g. etc/someconfig.cfg)
                init-files/   # Put any files in here that should be available to customize.sh
                overlays/     # Put your custom overlays (in .dts format) in here
                config.yaml   # Configure certain parameters of the armbian build in here
                customize.sh  # Script that's called right after the build so you can customize the image from within
        """.trimIndent()
    }
}
