package plugin.armbian

import com.charleskorn.kaml.Yaml
import com.charleskorn.kaml.decodeFromStream
import org.gradle.api.DefaultTask
import org.gradle.api.InvalidUserDataException
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import org.intellij.lang.annotations.Language
import java.io.File

abstract class ArmbianBuildTask : DefaultTask() {
    init {
        group = "armbian"
    }

    @get:Input
    abstract val device: Property<String>

    @TaskAction
    fun build() {
        val hostname = device.get()

        val userPatchPath = project.file("${project.buildDir}/userpatches/$hostname")
        val outputDir = project.file("${project.buildDir}/output/$hostname")
        val armbianOutputDir = project.rootProject.file("armbian/output")
        val configDir = project.file("devices/$hostname")

        val configFile = configDir.resolve("config.yaml")
        if (!configFile.isFile) {
            throw InvalidUserDataException("Config file for device $hostname not found")
        }
        if (!configDir.resolve("network-config.yaml").isFile) {
            throw IllegalStateException("No network-config.yaml for $hostname present")
        }
        if (!configDir.resolve("user-data.yaml").isFile) {
            throw IllegalStateException("No user-data.yaml for $hostname present")
        }

        val config = configFile.inputStream().use {
            Yaml.default.decodeFromStream<ArmbianConfig>(it)
        }


        project.delete(outputDir)
        val rootDeletePaths = mutableListOf<String>()
        if (armbianOutputDir.isDirectory) {
            rootDeletePaths.add(armbianOutputDir.absolutePath)
        }
        if (userPatchPath.isDirectory) {
            rootDeletePaths.add(userPatchPath.absolutePath)
        }
        if (rootDeletePaths.size != 0) {
            project.exec {
                executable("pkexec")
                args("rm", "-r")
                args(rootDeletePaths)
            }
        }
        project.mkdir("$userPatchPath/overlay/root")
        project.mkdir("$userPatchPath/overlay/init")
        project.mkdir("$userPatchPath/overlay/overlays")
        project.mkdir("$userPatchPath/overlay/cloud-init")

        project.file("$userPatchPath/lib.config").writeText(generateLibConfig(config.additionalPackages))
        project.file("$userPatchPath/customize-image.sh").apply {
            writeText(generateCustomizeScript(config.enableOverlays, config.kernelParameters))
            setExecutable(true)
        }
        project.copy {
            from(project.rootProject.file("resources/setup.sh"))
            into(userPatchPath.resolve("overlay"))
        }
        if (configDir.resolve("extra-files").isDirectory) {
            project.copy {
                includeEmptyDirs = true
                from("$configDir/extra-files")
                into("$userPatchPath/overlay/root")
            }
        }

        if (configDir.resolve("init-files").isDirectory) {
            project.copy {
                includeEmptyDirs = true
                from("$configDir/init-files")
                into("$userPatchPath/overlay/init")
            }
        }
        if (configDir.resolve("overlays").isDirectory) {
            project.copy {
                from("$configDir/overlays")
                into("$userPatchPath/overlay/overlays")
            }
        }
        if (configDir.resolve("customize.sh").isFile) {
            configDir.resolve("customize.sh").setExecutable(true)
            project.copy {
                from("$configDir/customize.sh")
                into("$userPatchPath/overlay")
            }
        }
        project.copy {
            from("$configDir/network-config.yaml", "$configDir/user-data.yaml")
            into("$userPatchPath/overlay/cloud-init")
            rename { it.split('.')[0] }
        }
        project.file("$userPatchPath/overlay/cloud-init/meta-data").writeText(generateMetaData(hostname))

        project.exec {
            workingDir("armbian")
            executable("pkexec")
            args(
                "./compile.sh",
                "BOARD=${config.board}",
                "BRANCH=${config.branch}",
                "RELEASE=${config.release}",
                "BUILD_MINIMAL=${config.buildMinimal.toYesNo()}",
                "BUILD_DESKTOP=${config.buildDesktop.toYesNo()}",
                "KERNEL_ONLY=no",
                "KERNEL_CONFIGURE=no",
                "COMPRESS_OUTPUTIMAGE=sha,gz",
                "DISABLE_IPV6=${config.disableIpv6}",
                "ROOTFS_TYPE=${config.rootFsType}",
                "BOOTSIZE=${config.bootSize}",
                "INSTALL_KSRC=${config.installKernelSource.toYesNo()}",
                "WIREGUARD=${config.includeLegacyWireguard.toYesNo()}",
                "USERPATCHES_PATH=$userPatchPath",
            )
        }

        val outputFiles = armbianOutputDir.resolve("images")
            .listFiles { file: File -> file.name.endsWith(".img.gz") && file.name.startsWith("Armbian") }
            ?: arrayOf<File>()
        if (outputFiles.size == 0) {
            throw IllegalStateException("No output file produced")
        } else if (outputFiles.size > 1) {
            throw IllegalStateException("More than one output file produced")
        }
        var outputFile = outputFiles[0]

        val user = System.getProperty("user.name")

        project.exec {
            executable("pkexec")
            args("chown", user, "-R", project.file("armbian"))
        }

        project.copy {
            from(outputFile)
            into(outputDir)
            rename { "Armbian_${config.board}_${config.branch}_${config.release}.img.gz" }
        }

        project.exec {
            executable("rm")
            args("-r", armbianOutputDir)
        }
    }

    companion object {
        @Language("Shell Script")
        fun generateCustomizeScript(enableOverlays: Collection<String>, kernelParameters: Map<String, String>): String {
            val commandLine = kernelParameters.entries
                .joinToString(" ") { "${it.key}=${it.value}".replace("\\", "\\\\").replace("/", "\\/") }
            return """
                    #!/bin/bash -e
                    
                    /tmp/overlay/setup.sh
                    
                    echo "- Enabling overlays..."
                    echo "overlays=${enableOverlays.joinToString(" ")}" >> /boot/armbianEnv.txt
                    echo "- Adding kernel parameters..."
                    sed -i 's/extraargs=/extraargs=$commandLine /' /boot/armbianEnv.txt
                """.trimIndent()
        }

        @Language("Shell Script")
        fun generateLibConfig(additionalPackages: Collection<String>) =
            "PACKAGE_LIST_ADDITIONAL=\"\$PACKAGE_LIST_ADDITIONAL cloud-init netplan.io ${
                additionalPackages.joinToString(" ")
            }\""

        @Language("yaml")
        fun generateMetaData(hostname: String) = """
            instance-id: "$hostname"
            local-hostname: "$hostname"
        """.trimIndent()

        private fun Boolean.toYesNo() = if (this) "yes" else "no"
    }
}
