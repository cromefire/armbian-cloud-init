package plugin.armbian

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.provider.ListProperty
import org.gradle.kotlin.dsl.create
import org.gradle.kotlin.dsl.register

interface ArmbianPluginExtension {
    val devices: ListProperty<String>
}

class ArmbianPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        val extension = project.extensions.create<ArmbianPluginExtension>("armbian")

        val armbianDevices: Collection<String> = project.file("devices")
            .listFiles()!!
            .filter { it.isDirectory }
            .filter { it.name.matches(ArmbianConstants.hostnameRegex) }
            .map { it.name }

        extension.devices.addAll(armbianDevices)

        for (deviceName in armbianDevices) {
            project.tasks.register<ArmbianBuildTask>("build-$deviceName") {
                device.set(deviceName)
            }
        }

        /*project.tasks.register("cleanArmbianRepo") {
            group = "armbian"

            doLast {
                project.exec {
                    workingDir(project.file("armbian"))
                    executable("pkexec")
                    args("git", "clean", "-dfx")
                }
            }
        }*/

        project.tasks.register<InitDeviceTask>("initDevice")
    }
}
