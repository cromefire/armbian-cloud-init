#!/bin/bash -e

INIT_FILES=/tmp/overlay/init-files

# Use this script for everything that needs to be customized on the image after the build.
# Put any files you need during this script into the init/ folder, they will be available to this script in the folder
# denoted by ${INIT_FILES}.

################
### Examples ###
################

## Upgrade everything
#echo "Upgrading packages..."
# Not upgrading armbian stuff
#apt-mark hold linux-image-current-* linux-dtb-current-* linux-u-boot-*-current armbian-firmware armbian-zsh armbian-config armbian-bsp-cli-*
# Get latest versions
#apt-get update
# Upgrade non-interactively
#apt-get dist-upgrade -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y
# Cleanup
#apt-get autoremove -y
#apt-get clean
#rm -r /var/lib/apt/lists
# Make armbian stuff upgradable again
#apt-mark unhold linux-image-current-* linux-dtb-current-* linux-u-boot-*-current armbian-firmware armbian-zsh armbian-config armbian-bsp-cli-*

## Disable user creation, if you create one via cloud-init
#echo "Disabling root user..."
# Disable the initial ssh setup:
#rm /root/.not_logged_in_yet
# Disable root login:
#passwd -l root
# Disable root login via SSH:
#echo "PermitRootLogin no" > /etc/ssh/sshd_config.d/50-no-root-login.conf
