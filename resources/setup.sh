#!/bin/bash -e

shopt -s nullglob

echo "- Configuring cloud-init..."
cp -r /tmp/overlay/cloud-init /boot/cloud-init
chmod -R 0400 /boot/cloud-init
if grep "extraargs=" /boot/armbianEnv.txt > /dev/null; then
  sed -i 's/extraargs=/extraargs=ds=nocloud;s=\/boot\/cloud-init\/ /' /boot/armbianEnv.txt
else
  echo "extraargs=ds=nocloud;s=/boot/cloud-init/" >> /boot/armbianEnv.txt
fi

echo "- Enabling overlays..."
for dts in /tmp/overlay/overlays/*.dts; do
  armbian-add-overlay "${dts}"
done

echo "- Copying extra files..."
for file in /tmp/overlay/root/*; do
  cp -vr "${file}" /
done

if [[ -x /tmp/overlay/customize.sh ]]; then
  echo "- Running user customization script..."
  /tmp/overlay/customize.sh
fi
